import { saveAs } from "file-saver";


async function readTextFile(file: File): Promise<string> {
    return new Promise((resolve) => {
        const reader = new FileReader();
        reader.addEventListener("load", function () {
            if (reader.result) {
                resolve(reader.result as string);
            }
        });
        reader.readAsText(file);
    });
}
export async function readJson(file: File): Promise<unknown> {
    return JSON.parse(await readTextFile(file));
}

export function saveAsJson(data: unknown, fileName: string): void {
    const json = JSON.stringify(data);
    const blob = new Blob([json], { type: "application/json" });
    saveAs(blob, fileName);
}