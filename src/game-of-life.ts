
function rebuildWorld(world: boolean[][], computeNewCell: (world: boolean[][], y: number, x: number) => boolean): boolean[][] {
    const newWorld = [];
    for (let y = 0; y < world.length; y++) {
        newWorld.push(Array(world[y].length));
        for (let x = 0; x < world[y].length; x++) {
            newWorld[y][x] = computeNewCell(world, y, x);
        }
    }
    return newWorld;
}

function getNbNeighborsAlive(world: boolean[][], y: number, x: number): number {
    let nbNeighborsAlive = 0;
    for (let ydelta = -1; ydelta < 2; ydelta++) {
        const neighborY = y + ydelta;
        if (neighborY >= 0 && neighborY < world.length) {
            for (let xdelta = -1; xdelta < 2; xdelta++) {
                const neighborX = x + xdelta;
                if (neighborX >= 0 && neighborX < world[neighborY].length
                    && !(neighborY == y && neighborX == x)
                    && world[neighborY][neighborX]) {
                    nbNeighborsAlive++;
                }
            }
        }
    }
    return nbNeighborsAlive;
}

function isAliveInNextGen(world: boolean[][], y: number, x: number): boolean {
    const nbNeighborsAlive = getNbNeighborsAlive(world, y, x);
    if (world[y][x] && nbNeighborsAlive == 2) {
        return true;
    }
    if (nbNeighborsAlive == 3) {
        return true;
    }
    return false;
}

export function rebuildEmptyWorld(world: boolean[][]): boolean[][] {
    return rebuildWorld(world, () => false);
}

export function rebuildRandomWorld(world: boolean[][], density: number): boolean[][] {
    return rebuildWorld(world, () => Math.random() < density);
}

export function buildNexGenerationWorld(world: boolean[][]): boolean[][] {
    return rebuildWorld(world, isAliveInNextGen);
}