import Vue from "vue";
import VueI18n from 'vue-i18n';
import App from "./App.vue";
import "bootstrap/dist/css/bootstrap.min.css";
import messages from "./messages.json";

Vue.use(VueI18n);
Vue.config.productionTip = false;

const i18n = new VueI18n({
  locale: navigator.language.split('-')[0],
  fallbackLocale: 'en',
  silentFallbackWarn: true,
  silentTranslationWarn: true,
  formatFallbackMessages: true,
  messages
});

new Vue({
  i18n,
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  render: h => h(App)
}).$mount("#app");
