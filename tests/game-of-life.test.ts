import * as gameOfLife from "../src/game-of-life";

test('rebuildEmptyWorld', () => {
  const newWorld = gameOfLife.rebuildEmptyWorld([
    [true, true, false],
    [false, true, true],
  ])
  expect(newWorld).toStrictEqual([
    [false, false, false],
    [false, false, false],
  ]);
});

test('rebuildRandomWorld full', () => {
  const newWorld = gameOfLife.rebuildRandomWorld([
    [false, false, false],
    [false, false, false],
  ], 1)
  expect(newWorld).toStrictEqual([
    [true, true, true],
    [true, true, true],
  ]);
});

test('rebuildRandomWorld empty', () => {
  const newWorld = gameOfLife.rebuildRandomWorld([
    [true, true, true],
    [true, true, true],
  ], 0)
  expect(newWorld).toStrictEqual([
    [false, false, false],
    [false, false, false],
  ]);
});

test('buildNexGenerationWorld surrounded dies', () => {
  const newWorld = gameOfLife.buildNexGenerationWorld([
    [true, true, true],
    [true, true, true],
  ])
  expect(newWorld).toStrictEqual([
    [true, false, true],
    [true, false, true],
  ]);
});

test('buildNexGenerationWorld square stays alive', () => {
  const newWorld = gameOfLife.buildNexGenerationWorld([
    [true, true, false],
    [true, true, false],
  ])
  expect(newWorld).toStrictEqual([
    [true, true, false],
    [true, true, false],
  ]);
});

test('buildNexGenerationWorld alone dies', () => {
  const newWorld = gameOfLife.buildNexGenerationWorld([
    [false, true, true],
    [false, false, false],
  ])
  expect(newWorld).toStrictEqual([
    [false, false, false],
    [false, false, false],
  ]);
});

test('buildNexGenerationWorld with 3 neighbors appears', () => {
  console.log("buildNexGenerationWorld with 3 neighbors appears")
  const newWorld = gameOfLife.buildNexGenerationWorld([
    [false, true, true],
    [false, false, true],
  ])
  expect(newWorld).toStrictEqual([
    [false, true, true],
    [false, true, true],
  ]);
});